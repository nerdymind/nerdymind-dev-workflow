##Get going with NerdyMind workflow

---

1. Install [Xcode](https://itunes.apple.com/us/app/xcode/id497799835?mt=12)
2. Install [Node.js](https://nodejs.org/en/)
    - install [Gulp](http://gulpjs.com/)
        - after node is installed, open terminal window and enter: `npm install -g gulp-cli` (**note:** may need to precede command with `sudo` if you receive permission errors)
    - install [Bower](http://bower.io/#getting-started)
        - after node is installed, open terminal window and enter: `npm install -g bower` (**note:** may need to precede command with `sudo` if you receive permission errors)
    - **sanity check**: at this point both `gulp` and `bower` commands should work from the terminal
3. Install [Composer](https://getcomposer.org/)
    1. open terminal window and enter: `curl -sS https://getcomposer.org/installer | php`
    2. open terminal window and enter: `sudo mv composer.phar /usr/local/bin/composer`
    3. **sanity check**: at this point `composer` command should work from the terminal
4. Install [Wordpress CLI](http://wp-cli.org/)
    1. open terminal window and enter: `curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar`
    2. in same window, enter: `chmod +x wp-cli.phar`
    3. and finally, enter: `sudo mv wp-cli.phar /usr/local/bin/wp`
    4. **sanity check**: at this point `wp` command should work from the terminal
5. Install [Bitbucket CLI](https://bitbucket.org/zhemao/bitbucket-cli)
    1. install **PIP** (python package manager). Open terminal window and enter: `easy_install pip` (**note:** may need to precede command with `sudo` if you receive permission errors)
    2. make sure using the latest version of **PIP**, open terminal window and enter: `pip install --upgrade pip` (**note:** may need to precede command with `sudo` if you receive permission errors)
    3. after PIP is installed, open terminal window and enter: `pip install bitbucket-cli` (**note:** may need to precede command with `sudo` if you receive permission errors)
    4. **sanity check**: at this point `bb` command should work from the terminal
6. Install MAMP Pro
    - configure MAMP pro (ask for details)
7. Install [Wordmove](https://github.com/welaika/wordmove)
    - open terminal window and enter: `gem install wordmove` (**note:** may need to precede command with `sudo` if you receive permission errors)
    - **sanity check**: at this point `wordmove` command should work from the terminal
8. Install [Macports](https://www.macports.org/install.php) - this is needed for packaged managment. Can also use **Homebrew** as an alternative. However, recommend **Macports** instead for easier package management.
    - install **sshpass**
        - after Macports is installed, open terminal window and enter: `sudo port install sshpass`
    - **sanity check**: at this point `sshpass` command should work from the terminal

---

##Add MAMP mySQL and PHP alias to bash profile:

- Open Terminal
- type: `sudo nano ~/.bash_profile ↵`
- Copy and paste the following:

    `export MAMP_PHP=/Applications/MAMP/bin/php/php5.6.10/bin`

    `export MAMP_BINS=/Applications/MAMP/Library/bin`

    `export USERBINS=~/bins`

    `export PSQL=/Applications/Postgres.app/Contents/Versions/9.5/bin`

    `export PATH="$USERBINS:$MAMP_PHP:$MAMP_BINS:$PSQL:$PATH"`


    - type: `ctrl+X ↵`
    - type: `Y ↵`
- Close terminal
- Re-open terminal, type: `sql ↵`
    - If SQL command prompt, success! Else troubleshoot.
- type: `ctrl+C ↵` to exit `sql` prompt

---

After above steps completed, should be ready to use our create-site script!
Grab the [create site.sh](https://bitbucket.org/nerdymind/createsite) file from the NerdyMind Bitbucket repo
Unzip and copy the createsite.sh into the your “Sites” folder (or wherever you plan to store the local development repositories).

Open a terminal window and `cd` into your “Sites” folder (or wherever you plan to store the local development repositories).
Within the root of your development site folder, type: `sh create site.sh` and follow the prompts.
